# About Me

I don't really like to repeat myself. But here are some profiles where you can find all sort of information about me:

- [Github](https://github.com/svenfinke)
- [Gitlab](https://gitlab.com/svenfinke)
- [Stackoverflow](https://stackoverflow.com/users/88446/svenfinke)
- [Website](https://www.svenfinke.com/)
- [Twitter](https://twitter.com/SvenFinke)
- [Xing](https://www.xing.com/profile/Sven_Finke3)
- [Linkedin](https://www.linkedin.com/in/sven-finke-0baa6220/)

And if all that is not enough, just open an issue in this repo or write me an [e-mail](mailto:sven.finke@gmail.com) :)